package tw.edu.hust.yn97024.sqlite;

import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	private Button btn_write, btn_reader, btn_clear;
	private SQLiteDatabase db;
	private Cursor cursor;

	/**
	 * Activity建立呼叫
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // TODO
        
		// 產生(讀取) SQLite
		
        // 建立資料表
		createTable("Name");
        
        // 宣告元件
        btn_write = (Button) findViewById(R.id.btn_write);
        btn_reader = (Button) findViewById(R.id.btn_reader);
        btn_clear = (Button) findViewById(R.id.btn_clear);
        
        // 點擊事件
        btn_write.setOnClickListener(this);
        btn_reader.setOnClickListener(this);
        btn_clear.setOnClickListener(this);
    }

	/**
	 * 建立資料表
	 * @param table 資料表名稱
	 * <br>
	 * <strong>範例欄位:</strong>
	 * <pre>
	 * _id INTEGER PRIMARY KEY AUTOINCREMENT
	 * name VARCHER(100) NOT NULL
	 * value INTEEGER NOT NULL DEFAULT 0
	 * </pre>
	 */
    private void createTable(String table) {
    	// TODO
	}

    /**
     * 產生Menu選單
     */
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

	/**
	 * 點擊事件
	 * @param v 元件
	 * <br>
	 * <ul>switch
	 * 	<li>case R.id.btn_write: 寫入資料</li>
	 * 	<li>case R.id.btn_reader: 讀取資料</li>
	 * 	<li>case R.id.btn_clear: 清除資料</li>
	 * </ul>
	 * 
	 */
	@Override
	public void onClick(View v) {
		// TODO
		
		switch( v.getId() ){
		case R.id.btn_write:
			// 寫入資料
			
			break;
		case R.id.btn_reader:
			// 宣告游標並讀取資料
			
			
			// 使用迴圈讀取游標資料
			
			break;
		case R.id.btn_clear:
			// 刪除資料表
			
			// 建立資料表
			
			break;
		}
	}

    
}
